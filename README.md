# Intro

This is a python library which emulates the behavior of the NoradBeacon ruby gem. This allows for tests to be written and results parsed in python instead of needing Ruby.

# Installation
 * Requires:
   * [PyCrypto](https://www.dlitz.net/software/pycrypto/)
   * [Requests](http://docs.python-requests.org/en/master/)
   * [PyYAML](http://pyyaml.org/)

# API Changes

I tried to keep the API as close to the Ruby implementation as possible. However, some deviations were made:
* Runner.results_file was replaced with Runner.get_results_filename()
* The Runner constructor only accepts the test program name (and optional timeout). Program arguments are passed via a call to Runner.set_args()
* Runner.parse_results was replaced with Runner.get_results(), which returns a list containing each line of program output.
* The NoradAPI class should not be intantiated from the wrapper script. Instead, the post_results method was moved to the Runner class. So just calling Runner.post_results() will post the results.

# Example

* First, create a new test using the norad CLI. See https://gitlab.com/norad/cli/blob/master/WALKTHROUGH.md
* Change directories to the new test directory. I'll use python-nmap-test as the example test name.
* Clone this repo (`git clone https://gitlab.com/norad/python-beacon.git`)
* Create a new wrapper script in python. In this example, I'll call it nmap_test.py
* The existing ruby wrapper script can be removed.
* Adjust the Dockerfile to:
	* Copy over the python-beacon library
	* Execute the python wrapper script instead of the ruby script.
* Build the test using the noard CLI

## nmap_test.py

```python
#!/usr/bin/python
import python-beacon as pybeacon
import sys
import re
import shlex


runner = pybeacon.Runner("nmap")
out_file = runner.get_results_filename()
nmap_args = shlex.split('--script ssl-dh-params --script-args vulns.showall'.split() + sys.argv[1:] + "-oN {results_file}".format(results_file=out_file))
runner.set_args(nmap_args)
runner.execute(True) #Since nmap writes to the results file, don't redirect stdout to the results file
results = runner.get_results()
status = 'pass'
for line in results:
    if re.match('State: VULNERABLE', line):
        status = 'fail'
        break

if status == 'fail':
    sir = '6.0'
else:
    sir = 'no_impact'
title = 'Diffie-Hellman group strength'
description = 'Weak ephemeral Diffie-Hellman parameter detection for SSL/TLS services.'
result = pybeacon.Result("1", status, "\n".join(results), title, description, sir)
runner.add_result(result)
runner.post_results()
```

## manifest.yml

```yaml
registry: norad-registry.cisco.com:5000
name: python-nmap-test
version: latest
prog_args: '-p %{port} %{target}'
category: blackbox
test_types:
  - ssl_crypto
```

## Dockerfile

```
# Set the base image to nmap image
FROM norad-registry.cisco.com:5000/nmap:0.0.1

# File Author / Maintainer
MAINTAINER Phil Behnke <phibehnk@cisco.com>

# Install python-beacon dependencies
RUN apt-get update && apt-get install -y python-yaml python-crypto python-requests

# Copy over python-beacon
COPY ./python-beacon /python-beacon
# Copy our test file
COPY ./nmap_test.py /
# Run the test
RUN chmod a+x /nmap_test.py
ENTRYPOINT ["/nmap_test.py"]
```

# To-do

* Implement the MultiRunner class.
