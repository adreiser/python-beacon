#!/usr/bin/python

import os
import json
import hashlib
import hmac
import time

class ResultSet:

	def __init__(self, assessment_url = None):
		self.results = []
		self.timestamp = None
		self.url = assessment_url
		if assessment_url == None:
			paths = os.environ['ASSESSMENT_PATHS']
			assessments = json.loads(paths)
			self.url = assessments.pop(0)['assessment']

	def add(self, result):
		self.results.append(result)

	def payload(self):
		if self.timestamp == None:
			self.timestamp = int(time.time())
		ret = {'results': self.results, 'timestamp': self.timestamp}
		return json.dumps(ret, default=lambda o: o.__dict__, separators=(',',':'))

	def compute_signature(self):
		hexkey = os.environ['NORAD_SECRET']
		mac = hmac.new(hexkey, self.payload(), hashlib.sha256)
		return mac.hexdigest()

