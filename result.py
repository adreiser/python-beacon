#!/usr/bin/python

import hashlib
#import json

class Result:

	def __init__(self, nid, status, output, title, description, sir = 'unevaluated', text_to_fingerprint = None):
		self.nid = nid
		self.status = status
		self.output = output
		self.title = title
		self.description = description
		self.sir = self.cvss_to_sir(sir)
		self.signature = self.compute_signature(text_to_fingerprint)

	def cvss_to_sir(self, sir):

		try:
			sir = float(sir)
		except ValueError:
			#sir is already a string
			return sir

		if sir < 4:
			return 'low'
		elif sir < 7:
			return 'medium'
		elif sir < 9:
			return 'high'
		elif sir <= 10:
			return 'critical'
		else:
			return 'unevaluated'

	def compute_signature(self, text_to_fingerprint):
		if text_to_fingerprint == None:
			text_to_fingerprint = self.output

		sha = hashlib.sha256()
		sha.update(self.nid)
		sha.update(self.title)
		sha.update(text_to_fingerprint)
		return sha.hexdigest()

