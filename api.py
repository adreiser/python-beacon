#!/usr/bin/python

import requests
import os

class NoradAPI:

	def __init__(self):
		self.norad_root = os.environ['NORAD_ROOT']

	def post_results(self, results):
		headers = { 'NORAD-SIGNATURE': results.compute_signature(), 'Content-Type': 'application/json'}
		url = self.norad_root + results.url
		r = requests.post(url, headers=headers, data=results.payload())
