#!/usr/bin/python

import os
import subprocess
import tempfile
import time
import re

from resultset import ResultSet
from container_options import ContainerOptions
from api import NoradAPI

class Runner:

	def __init__(self, prog, timeout = 600):
		self.prog = prog
		self.timeout = timeout
		self.result_set = ResultSet()
		self.exit_code = None
		(self.results_fd, self.results_filename) = tempfile.mkstemp(prefix="norad.results")

	def set_args(self, args):
		self.args = self.format_args(args)

	def get_results_filename(self):
		return self.results_filename

	def add_result(self, result):
		self.result_set.add(result)

	def post_results(self):
		api = NoradAPI()
		api.post_results(self.result_set)

	def execute(self, stdout_results=False):
		args = [self.prog] + self.args
		if stdout_results:
			process = subprocess.Popen(args)
		else:
			process = subprocess.Popen(args, stdout=self.results_fd)
		self.exit_code = self.poll_for_exit(process)
		os.close(self.results_fd)

	def poll_for_exit(self, process):
		start = time.time()
		end = start + self.timeout
		while True:
			result = process.poll()
			if result is not None:
				return result
			if time.time() >= end:
				print 'Beacon Timeout reached. Stopping the process...'
				process.kill()
			#poll once every second
			time.sleep(1)

	def get_results(self):
		with open(self.results_filename) as f:
			content = f.readlines()
		return content


	def format_args(self, args):
		formatted = []
		container_options = ContainerOptions()
		options = container_options.decrypt_and_load()
		try:
			container_name = os.environ['NORAD_SECURITY_CONTAINER_NAME']
		except KeyError:
			container_name = None

		if options and container_name:
			try:
				local_opts = options[container_name]
			except KeyError:
				local_opts = {}
		else:
			local_opts = {}

		for i in args:
			match = re.match('%\{([a-zA-Z_][a-zA-Z_0-9]*)\}', i)
			if match != None:
				key = match.group(1)
				if key != None:
					try:
						formatted.append(local_opts[key])
					except KeyError:
						formatted.append(i)
				else:
					formatted.append(i)
			else:
				formatted.append(i)
		return formatted