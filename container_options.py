#!/usr/bin/python

import os
import base64
from Crypto.Cipher import AES
import yaml

class ContainerOptions:

	def __init__(self, container_file_uri='/containers.yml.enc'):
		self.container_file_uri = container_file_uri

	def decrypt_and_load(self):
		aes_key = self.load_key_from_env()
		if aes_key:
			data = self.decrypt_containers_file(aes_key)
			if data:
				return yaml.load(data)
			else:
				return None
		else:
			return None
		

	def load_key_from_env(self):
		try:
			key = os.environ['NORAD_CONTAINERS_FILE_KEY']
		except KeyError:
			return None
		key = base64.b64decode(key)
		return key

	def decrypt_containers_file(self, key):
		unpad = lambda s : s[0:-ord(s[-1])]
		try:
			with open(self.container_file_uri) as f:
				# First 16 bytes of this file is the IV
				iv = f.read(16)
				ciphertext = f.read()
		except IOError:
			return None
		aes = AES.new(key, AES.MODE_CBC, iv)
		plaintext = aes.decrypt(ciphertext)
		#file is encrypted with openssl, which uses pkcs5 padding.
		return unpad(plaintext)
